<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package pluginever
 */

get_header();
?>

	<section id="primary" class="content-area container">
		<main id="main" class="site-main">

		<?php  if ( get_post_type() === 'docs' ): ?>
			<div class="wedocs-single-wrap pluginever-doc-search-page">
				<?php wedocs_get_template_part( 'docs', 'sidebar' ); ?>
				<div class="wedocs-single-content">
					<header class="page-header">
						<h1 class="page-title">
							<?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'pluginever' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>
					</header><!-- .page-header -->

					<?php
						if ( have_posts() ) {
							while ( have_posts() ) {
								the_post();

								get_template_part( 'template-parts/content-doc', 'search' );
							}

							pluginever_page_navs();
						} else {
							get_template_part( 'template-parts/content-doc', 'none' );
						}
					?>
				</div>
			</div>
		<?php else: ?>
			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h1 class="page-title">
						<?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Search Results for: %s', 'pluginever' ), '<span>' . get_search_query() . '</span>' );
						?>
					</h1>
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'template-parts/content', 'search' );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		<?php endif; ?>


		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
