module.exports = function (grunt) {
    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        // Minify .js files.
        uglify: {
            options: {
                ie8: true,
                parse: {
                    strict: false
                },
                output: {
                    comments: /@license|@preserve|^!/
                }
            },
            bundle:{
                files: {
                    'js/bundle.js': ['js/navigation.js', 'js/skip-link-focus-fix.js', 'js/slick.js', 'js/simple-lightbox.js','js/pluginever.com'],
                }
            }
        },


        /*Sass Task*/
        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    sourceMap: true
                },
                files: {
                    'sass/style.css' : 'sass/style.scss'
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'style.css': ['sass/style.css']
                }
            }
        },

        /*Autoprefixer*/
        autoprefixer: {
            options: {
                browsers: ['last 2 versions']
            },
            //prefix all files
            multiple_files: {
                expand: true,
                flatten: true,
                src: 'compiled/*.css',
                dest: ''
            }
        },

        /*Watch task*/
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass', 'cssmin', 'autoprefixer']
            },
            js: {
                files: [ 'js/**.js'],
                tasks: ['uglify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.registerTask('default', ['watch']);

};