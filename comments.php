<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pluginever
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

    <div id="comments">
        <div class="comments-wrap">
            <?php if ( have_comments() ) : ?>
                <?php do_action( 'pluginever_comments_title' ); ?>
                <ol class="commentlist"><?php wp_list_comments( "callback=pluginever_comment" ); ?></ol>
            <?php endif; ?>

            <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
                <nav id="comment-nav-below" role="navigation">
                    <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'pluginever' ) ); ?></div>
                    <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'pluginever' ) ); ?></div>
                </nav>
            <?php endif; // check for comment navigation ?>

            <?php if ( ! comments_open() && get_comments_number() ) : ?>
                <p class="no-comments"><?php _e( 'Comments are closed.' , 'pluginever' ); ?></p>
            <?php endif; ?>

            <?php comment_form(  ); ?>
        </div><!-- .comments-wrap -->
    </div><!-- #comments -->

</div><!-- #comments -->
