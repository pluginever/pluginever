<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package pluginever
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function pluginever_body_classes( $classes ) {
    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    // Adds a class of no-sidebar when there is no sidebar present.
//	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
//		$classes[] = 'no-sidebar';
//	}

    global $post;

    if ( is_front_page() || is_singular( 'download' ) || ( is_page() && 'on' == get_post_meta( $post->ID, 'hide_header_bg', true ) ) ) {
        $classes[] = 'no-header-bg';
    }

    return $classes;
}

add_filter( 'body_class', 'pluginever_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function pluginever_pingback_header() {
    if ( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
    }
}

add_action( 'wp_head', 'pluginever_pingback_header' );


function pluginever_page_navs( $query = false ) {
    global $wp_query;
    if ( $query ) {
        $temp_query = $wp_query;
        $wp_query   = $query;
    }
    // Return early if there's only one page.
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
        return;
    } ?>
    <div class="page-navigation">
        <?php
        $big = 999999999; // need an unlikely integer
        echo paginate_links( array(
            'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'  => '?paged=%#%',
            'current' => max( 1, get_query_var( 'paged' ) ),
            'total'   => $wp_query->max_num_pages
        ) );
        ?>
    </div>
    <?php
    if ( isset( $temp_query ) ) {
        $wp_query = $temp_query;
    }
}