<?php

/**
 * Custom excerpt length
 *
 * @since pluginever 1.0
 */
function pluginever_excerpt_length( $length ) {
//    if ( ! is_post_type_archive( 'download' ) ) {
//        return 90;
//    }

    return $length;
}

add_filter( 'excerpt_length', 'pluginever_excerpt_length', 999 );


/**
 * Replace default excerpt [...]
 *
 * @since pluginever 1.0
 */
function pluginever_new_excerpt_more( $more ) {
    return '...';
}

add_filter( 'excerpt_more', 'pluginever_new_excerpt_more' );


/**
 * Custom excerpt read more link on archives
 *
 * @since pluginever 1.0
 */
function pluginever_custom_excerpt( $text ) {
    global $post;
    if( !is_single()){
        return $text . '<p><a class="more-link button secondary display-block" href="' . get_permalink() . '"> ' . __( 'Read More', 'pluginever' ) . ' </a></p>';
    }

    return $text;
}

add_filter( 'the_excerpt', 'pluginever_custom_excerpt' );

/**
 * Custom read more link
 *
 * @since pluginever 1.0
 */
function pluginever_modify_read_more_link() {
    return '<a class="more-link  button secondary display-block" href="' . get_permalink() . '"> ' . __( 'Read More', 'pluginever' ) . ' </a>';
}

add_filter( 'the_content_more_link', 'pluginever_modify_read_more_link' );

/**
 * Removes the built-in styles in the Subtitles plugin.
 *
 * @since pluginever 1.0
 */
function pluginever_remove_subtitles_styles() {
    if ( class_exists( 'Subtitles' ) && method_exists( 'Subtitles', 'subtitle_styling' ) ) {
        remove_action( 'wp_head', array( Subtitles::getInstance(), 'subtitle_styling' ) );
    }
}

add_action( 'template_redirect', 'pluginever_remove_subtitles_styles' );

/**
 * Add subtitle support to downloads and portfolio items
 *
 * @since pluginever 1.0
 */
function pluginever_add_subtitles_support() {
    add_post_type_support( 'download', 'subtitles' );
}

add_action( 'init', 'pluginever_add_subtitles_support' );

/**
 * Remove automatic output of subtitles
 *
 * @since pluginever 1.0
 */
function pluginever_subtitles_mod_supported_views( $subtitle ) {
    return '';
}

add_filter( 'subtitle_view_supported', 'pluginever_subtitles_mod_supported_views' );

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 *
 * @return string The filtered title.
 */
function pluginever_wp_title( $title, $sep ) {
    if ( is_feed() ) {
        return $title;
    }

    global $page, $paged;

    // Add the blog name
    $title .= get_bloginfo( 'name', 'display' );

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title .= " $sep $site_description";
    }

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 ) {
        $title .= " $sep " . sprintf( __( 'Page %s', 'pluginever' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'pluginever_wp_title', 10, 2 );


/**
 * Redirect to login page
 */
function pluginever_redirect_to_login() {
    $obj = get_queried_object();
    $my_account = get_page_by_path( 'my-account' );
    
    if ( ! is_user_logged_in() && ( is_page( 'my-account' ) || ( is_page() && $my_account && $obj->post_parent === $my_account->ID ) ) ) {
        $login = get_page_by_path( 'login' );

        if ( $login ) {
            wp_redirect( get_permalink( $login ) );
            exit;
        }
    }

    if ( is_user_logged_in() && ( is_page( 'login' ) || is_page( 'register' ) ) && $my_account ) {
        wp_redirect( get_permalink( $my_account ) );
        exit;
    }
}

add_action( 'template_redirect', 'pluginever_redirect_to_login' );

function pluginever_redirect_wp_login_to_login() {
    global $pagenow;
    $is_action = isset( $_REQUEST['action'] ) && $_REQUEST['action'] !== 'register' ? true : false;
    $interim_login = isset( $_REQUEST['interim-login'] ) ? true : false;

    if( ! $is_action && ! $interim_login && 'wp-login.php' == $pagenow ) {
        if ( $_REQUEST['action'] === 'register' ) {
            $register = get_page_by_path( 'register' );
            wp_redirect( get_permalink( $register ), 301 );
            exit;
        } else {
            $login = get_page_by_path( 'login' );
            wp_redirect( get_permalink( $login ), 301 );
            exit;
        }
    }
}
add_action( 'init', 'pluginever_redirect_wp_login_to_login'  );

function pluginever_wp_login_css() {
    ?>
    <style type="text/css">
        body.login {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/header-bg.svg);
            background-repeat: no-repeat;
            background-size: cover;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/pluginever-logo.svg);
            height:65px;
            width:320px;
            background-size: 320px 65px;
            background-repeat: no-repeat;
        }
    </style>
    <?php
}
add_action( 'login_enqueue_scripts', 'pluginever_wp_login_css' );

function pluginever_check_recaptcha_login_form() {
    if ( ! empty( $_POST['edd_action'] ) && 'user_login' === $_POST['edd_action'] && ! empty( $_POST['edd_login_nonce'] ) && wp_verify_nonce( $_POST['edd_login_nonce'], 'edd-login-nonce' ) ) {
        if ( ! empty( $_POST['g-recaptcha-response'] )  ) {
            $is_valid = pluginever_check_recaptcha( $_POST['g-recaptcha-response'] );
            if ( ! $is_valid ) {
                edd_set_error( 'recaptcha_invalid', __( 'Invalid reCaptcha. Please try again.', 'pluginever' ) );
            }
        } else {
            edd_set_error( 'recaptcha_invalid', __( 'Invalid reCaptcha. Please try again.', 'pluginever' ) );
        }

        $errors = edd_get_errors();
        
        if ( $errors ) {
            $_POST['edd_login_nonce'] = false;
        }
    }
}
add_action( 'init', 'pluginever_check_recaptcha_login_form', 0 );


function pluginever_check_recaptcha( $resp ) {
    $secretKey = "6LeH1LgUAAAAAFLe7QbYig047Top-0ClI5wNehEm";

    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode( $secretKey ) .  '&response=' . urlencode( $resp );

    $response = wp_remote_get( $url );

    if ( is_array( $response ) && ! is_wp_error( $response ) ) {
        $body         = $response['body'];
        $responseKeys = json_decode( $body, true );

        if( ! $responseKeys['success'] ) {
            return false;
        }
    } else {
        return false;
    }

    return true;
}

function pluginever_check_recaptcha_register_form() {
    if ( ! empty( $_POST['g-recaptcha-response'] )  ) {
        $is_valid = pluginever_check_recaptcha( $_POST['g-recaptcha-response'] );
        if ( ! $is_valid ) {
            edd_set_error( 'recaptcha_invalid', __( 'Invalid reCaptcha. Please try again.', 'pluginever' ) );
        }
    } else {
        edd_set_error( 'recaptcha_invalid', __( 'Invalid reCaptcha. Please try again.', 'pluginever' ) );
    }
}

add_action( 'edd_process_register_form', 'pluginever_check_recaptcha_register_form' );

function pluginever_setup_options() {
    $subscriber = get_role( 'subscriber' );
    $subscriber->add_cap( 'view_ticket', true );
    $subscriber->add_cap( 'create_ticket', true );
    $subscriber->add_cap( 'close_ticket', true );
    $subscriber->add_cap( 'reply_ticket', true );
    $subscriber->add_cap( 'attach_files', true );
}

add_action( 'after_switch_theme', 'pluginever_setup_options' );



function pluginever_admin_bar(){
    return current_user_can( 'edit_posts' );
}
add_filter('show_admin_bar', 'pluginever_admin_bar');




/**
 * Custom comment output
 *
 * @since pluginever 1.0
 */
function pluginever_comment( $comment, $args, $depth ) { $GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

    <div class="comment-block" id="comment-<?php comment_ID(); ?>">

        <div class="comment-info">
            <div class="comment-author vcard">
                <div class="vcard-wrap">
                    <?php echo get_avatar( $comment->comment_author_email, 100 ); ?>
                </div>
            </div>

            <div class="comment-text">
                <div class="comment-meta commentmetadata">
                    <?php printf( __( '<cite class="fn">%s</cite>', 'pluginever' ), get_comment_author_link() ); ?>

                    <div class="comment-time">
                        <?php
                        printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
                            esc_url( get_comment_link( $comment->comment_ID ) ),
                            get_comment_time( 'c' ),
                            /* translators: 1: date, 2: time */
                            sprintf( __( '%1$s at %2$s', 'pluginever' ), get_comment_date(), get_comment_time() )
                        );
                        ?>
                        <?php edit_comment_link( '<i class="icon-edit"></i>', '' ); ?>
                    </div>
                </div>
                <?php comment_text(); ?>

                <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
        </div>

        <?php if ( $comment->comment_approved == '0' ) : ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'pluginever' ); ?></em>
        <?php endif; ?>
    </div>
    <?php
}

function pluginever_comments_section_title() {
    echo "<h3 id='comments-title'>";
    printf( _nx( '1 Comment', '%1$s Comments', get_comments_number(), 'comments title', 'pluginever' ), number_format_i18n( get_comments_number() ) );
    echo "</h3>";
}
add_filter( 'pluginever_comments_title', 'pluginever_comments_section_title', 5 );

function pluginever_comments_google_recaptcha($submit_field) {
    $submit_field['submit_field'] = '<p><script src="https://www.google.com/recaptcha/api.js" async defer></script><span class="g-recaptcha" data-sitekey="6LeH1LgUAAAAAE9wTYVuTB69pb4xa2fHZZX-tpKm"></span></p>' . $submit_field['submit_field'];
    return $submit_field;
}
add_filter('comment_form_defaults','pluginever_comments_google_recaptcha');

function pluginever_comments_verify_google_recaptcha() {
    $recaptcha = $_POST['g-recaptcha-response'];

    if ( empty( $recaptcha ) ) {
        wp_die( "<b>ERROR:</b> please select <b>I'm not a robot!</b><p><a href='javascript:history.back()'>« Back</a></p>" );
    } else if ( ! pluginever_check_recaptcha( $recaptcha ) ) {
        wp_die( '<b>Go away SPAMMER!</b>' );
    }
}
add_action('pre_comment_on_post', 'pluginever_comments_verify_google_recaptcha');


remove_filter('get_post_metadata', 'dwqa_disable_wpdevart_facebook_comment', 10);