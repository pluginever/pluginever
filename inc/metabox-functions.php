<?php

function cmb2_download_metaboxes() {
    $cmb = new_cmb2_box( array(
        'id'           => 'download-metabox',
        'title'        => __( 'Download Settings', 'cmb2' ),
        'object_types' => array( 'download', ), // Post type
        'context'      => 'normal',
        'priority'     => 'high',
        'show_names'   => true, // Show field names on the left
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb->add_field( array(
        'name' => __( 'Documentation URL', 'cmb2' ),
        'id'   => 'pluginever_documentation_link',
        'type' => 'text_medium',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Demo URL', 'cmb2' ),
        'id'   => 'array_demo_field',
        'type' => 'text_url',
    ) );

    $cmb->add_field( array(
        'name'       => 'Screen shoots',
        'id'         => 'screenshoots',
        'type'       => 'file_list',
        'query_args' => array( 'type' => 'image' ), // Only images attachment
        'text'       => array(
            'add_upload_files_text' => 'Add Screenshoots', // default: "Add or Upload Files"
            'remove_image_text'     => 'Remove Screenshoot', // default: "Remove Image"
            'file_text'             => 'Screenshoot', // default: "File:"
            'file_download_text'    => 'Download', // default: "Download"
            'remove_text'           => 'Remove', // default: "Remove"
        ),
    ) );

}

add_action( 'cmb2_admin_init', 'cmb2_download_metaboxes' );


function cmb2_page_metaboxes() {
    $cmb = new_cmb2_box( array(
        'id'           => 'page-metabox',
        'title'        => __( 'Page Settings', 'cmb2' ),
        'object_types' => array( 'page', ), // Post type
        'context'      => 'side',
        'priority'     => 'high',
        'show_names'   => true, // Show field names on the left
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb->add_field( array(
        'name' => 'No header BG',
        'desc' => 'Hide header background image',
        'id'   => 'hide_header_bg',
        'type' => 'checkbox',
    ) );

}
add_action( 'cmb2_admin_init', 'cmb2_page_metaboxes' );