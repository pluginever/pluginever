<?php
function row_callback( $atts, $content ) {
    $atts = shortcode_atts( array(
        'style' => '',
        'class' => '',
    ), $atts, 'row' );

    $classes = 'row';
    if($atts['class']){
        $classes .= ' '. sanitize_html_class($atts['class']);
    }


    $html = '<div class="'.$classes.'" style="'.$atts['style'].'">';
    $html .= do_shortcode( $content );
    $html .= '</div>';

    return $html;
}

add_shortcode( 'row', 'row_callback' );


function column_callback( $atts, $content ) {
    $atts = shortcode_atts( array(
        'col'   => '',
        'style' => '',
        'class' => '',
    ), $atts, 'column' );
    var_dump($atts);
    $classes = empty( $atts['col'] ) ? 'col' : sanitize_html_class('col-md-' . $atts['col']);
    if($atts['class']){
        $classes .= ' '. sanitize_html_class($atts['class']);
    }


    $html = '<div class="'.$classes.'" style="'.$atts['style'].'">';
    $html .= do_shortcode( $content );
    $html .= '</div>';

    return $html;
}

add_shortcode( 'column', 'column_callback' );