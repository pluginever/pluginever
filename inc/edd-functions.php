<?php
/**
 * Define download archive page url
 */
if ( ! defined( 'EDD_SLUG' ) ) {
    define( 'EDD_SLUG', 'plugins' );
}
remove_filter( 'the_content', 'edd_after_download_content' );

/**
 * Overwrite pricing area
 *
 * @param int $download_id
 * @param array $args
 *
 * @since 1.0.0
 */
function pluginever_purchase_variable_pricing( $download_id = 0, $args = array() ) {
    global $edd_displayed_form_ids;

    // If we've already generated a form ID for this download ID, append -#
    $form_id = '';
    if ( $edd_displayed_form_ids[ $download_id ] > 1 ) {
        $form_id .= '-' . $edd_displayed_form_ids[ $download_id ];
    }

    $variable_pricing = edd_has_variable_prices( $download_id );

    if ( ! $variable_pricing ) {
        return;
    }

    $prices = apply_filters( 'edd_purchase_variable_prices', edd_get_variable_prices( $download_id ), $download_id );

    // If the price_id passed is found in the variable prices, do not display all variable prices.
    if ( false !== $args['price_id'] && isset( $prices[ $args['price_id'] ] ) ) {
        return;
    }

    $type   = edd_single_price_option_mode( $download_id ) ? 'checkbox' : 'radio';
    $mode   = edd_single_price_option_mode( $download_id ) ? 'multi' : 'single';
    $schema = edd_add_schema_microdata() ? ' itemprop="offers" itemscope itemtype="http://schema.org/Offer"' : '';

    // Filter the class names for the edd_price_options div
    $css_classes_array = apply_filters( 'edd_price_options_classes', array(
        'edd_price_options',
        'edd_' . esc_attr( $mode ) . '_mode'
    ), $download_id );

    // Sanitize those class names and form them into a string
    $css_classes_string = implode( array_map( 'sanitize_html_class', $css_classes_array ), ' ' );

    if ( edd_item_in_cart( $download_id ) && ! edd_single_price_option_mode( $download_id ) ) {
        return;
    }

    do_action( 'edd_before_price_options', $download_id ); ?>
    <div class="<?php echo esc_attr( rtrim( $css_classes_string ) ); ?>">
        <ul>
            <?php
            if ( $prices ) :
                $checked_key = isset( $_GET['price_option'] ) ? absint( $_GET['price_option'] ) : edd_get_default_variable_price( $download_id );
                foreach ( $prices as $key => $price ) :
                    echo '<li id="edd_price_option_' . $download_id . '_' . sanitize_key( $price['name'] ) . $form_id . '"' . $schema . '>';
                    echo '<label for="' . esc_attr( 'edd_price_option_' . $download_id . '_' . $key . $form_id ) . '">';
                    echo '<input type="' . $type . '" ' . checked( apply_filters( 'edd_price_option_checked', $checked_key, $download_id, $key ), $key, false ) . ' name="edd_options[price_id][]" id="' . esc_attr( 'edd_price_option_' . $download_id . '_' . $key . $form_id ) . '" class="' . esc_attr( 'edd_price_option_' . $download_id ) . '" value="' . esc_attr( $key ) . '" data-price="' . edd_get_price_option_amount( $download_id, $key ) . '"/>';

                    $item_prop = edd_add_schema_microdata() ? ' itemprop="description"' : '';

                    // Construct the default price output.
                    $price_output = '<span class="edd_price_option_price">' . edd_currency_filter( edd_format_amount( $price['amount'] ) ) . '</span><span class="edd_price_option_name"' . $item_prop . '>' . esc_html( $price['name'] ) . '</span>';

                    // Filter the default price output
                    $price_output = apply_filters( 'edd_price_option_output', $price_output, $download_id, $key, $price, $form_id, $item_prop );

                    echo '<span class="edd_price_option_wrap">';
                    // Output the filtered price output
                    echo $price_output;
                    echo '</span>';

                    if ( edd_add_schema_microdata() ) {
                        echo '<meta itemprop="price" content="' . esc_attr( $price['amount'] ) . '" />';
                        echo '<meta itemprop="priceCurrency" content="' . esc_attr( edd_get_currency() ) . '" />';
                    }

                    echo '</label>';
                    do_action( 'edd_after_price_option', $key, $price, $download_id );
                    echo '</li>';
                endforeach;
            endif;
            do_action( 'edd_after_price_options_list', $download_id, $prices, $type );
            ?>
        </ul>
    </div><!--end .edd_price_options-->
    <?php
    do_action( 'edd_after_price_options', $download_id );
}

remove_action( 'edd_purchase_link_top', 'edd_purchase_variable_pricing', 10 );
add_action( 'edd_purchase_link_top', 'pluginever_purchase_variable_pricing', 10, 2 );


function pluginever_download_extra_buttons( $download_id ) {
    if ( ! is_singular( 'download' ) ) {
        return false;
    }
    $demo_url          = get_post_meta( $download_id, 'array_demo_field', true );
    $documentation_url = get_post_meta( $download_id, 'pluginever_documentation_link', true );
    if ( ! empty( $demo_url ) ) {
        echo sprintf( '<a href="%s" class="button secondary download-meta-button" target="_blank">Live Demo</a>', $demo_url );
    }

    if ( ! empty( $documentation_url ) ) {
        echo sprintf( '<a href="%s" class="button secondary download-meta-button" target="_blank">Documentation</a>', $documentation_url );
    }
}

add_action( 'edd_purchase_link_end', 'pluginever_download_extra_buttons' );

function pluginever_print_ga_script_payment_receipt_after_table($payment, $edd_receipt_args) {

    if ( $edd_receipt_args['payment_id'] ) {
        // Use a meta value so we only send the beacon once.
        if (get_post_meta( $payment->ID, 'edd_ga_beacon_sent', true)) {
            return;
        }

        $grand_total = edd_get_payment_amount( $payment->ID );

        ?>
        <script type="text/javascript">

            if (typeof ga != "undefined") {
                ga('require', 'ecommerce', 'ecommerce.js');

                ga('ecommerce:addTransaction', {
                    'id': '<?php echo esc_js(edd_get_payment_number( $payment->ID )); ?>', // Transaction ID. Required.
                    'affiliation': '<?php echo esc_js(get_bloginfo('name')); ?>', // Affiliation or store name.
                    'revenue': '<?php echo $grand_total ? esc_js($grand_total) : '0'; ?>', // Grand Total.
                    'shipping': '0', // Shipping.
                    'tax': '<?php echo edd_use_taxes() ? esc_js(edd_payment_tax( $payment->ID )) : '0'; ?>' // Tax.
                });

                <?php if ( $edd_receipt_args[ 'products' ] ) {
                $cart = edd_get_payment_meta_cart_details( $payment->ID, true );
                if ($cart) {
                foreach ( $cart as $key => $item ) {
                if( empty( $item['in_bundle'] ) ) {

                $price_id = edd_get_cart_item_price_id( $item );
                $itemname = $item['name'];
                if( ! is_null( $price_id ) ) {
                    $itemname .= ' - '.edd_get_price_option_name( $item['id'], $price_id );
                }
                ?>
                ga('ecommerce:addItem', {
                    'id': '<?php echo esc_js(edd_get_payment_number( $payment->ID )); ?>', // Transaction ID. Required.
                    'name': '<?php echo esc_js($itemname); ?>',  // Product name. Required.
                    'sku': '<?php echo esc_js(edd_use_skus() ? edd_get_download_sku( $item['id'] ) : $item['id']); ?>',                 // SKU/code.
                    //'category': 'Example',  - Category or variation.
                    'price': '<?php echo esc_js($item['item_price']); ?>', // Unit price.
                    'quantity': '<?php echo esc_js($item['quantity']); ?>' // Quantity.
                });

                <?php
                }}}}

                ?>
                ga('ecommerce:send');
            }
        </script>
        <?php

        update_post_meta( $payment->ID, 'edd_ga_beacon_sent', true );
    }

}

add_action('edd_payment_receipt_after_table', 'pluginever_print_ga_script_payment_receipt_after_table', 10, 2);
