<?php
/**
 * Enqueue scripts and styles.
 */
function pluginever_scripts() {

    wp_deregister_style( 'edd-styles' );

    wp_enqueue_style( 'pluginever-style', get_stylesheet_uri(), array(), time() );

    wp_enqueue_script( 'pluginever', get_template_directory_uri() . '/js/bundle.js', array( 'jquery' ), time(), true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

add_action( 'wp_enqueue_scripts', 'pluginever_scripts', 99 );