<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package pluginever
 */

get_header();
?>

    <div id="primary" class="content-area container">
        <main id="main" class="site-main">

            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php get_template_part( 'partials/account', 'header' ); ?>
                    <div class="entry-content row">
                        <?php get_template_part( 'partials/account', 'sidebar' ); ?>
                        <div class="col-md-9">
                            <div class="my-account-content">
                                <?php
                                the_content();
                                ?>
                            </div>
                        </div>
                    </div><!-- .entry-content -->

                </article><!-- #post-<?php the_ID(); ?> -->

            <?php endwhile; // End of the loop.?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
