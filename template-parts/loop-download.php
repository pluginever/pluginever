<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pluginever
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-4 plugin-card'); ?>>
    <div class="plugin-card-inner">
        <header class="entry-header">
            <?php
            pluginever_post_thumbnail('download-thumb');
            ?>
        </header><!-- .entry-header -->
    
    
        <div class="entry-content">
            <?php
            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            the_excerpt();
            ?>
        </div><!-- .entry-content -->
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
