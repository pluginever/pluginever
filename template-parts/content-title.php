<?php
/**
 * Template for displaying post and page titles in the header.
 *
 * @package Checkout
 * @since Checkout 1.0
 */
?>

<?php if (!is_front_page()  || !is_singular(array('post', 'download'))) { ?>

<div class="hero-title">
	<div class="hero-title-inside container">
		<h1 class="entry-title">
			<?php
				if ( is_category() ) :
					single_cat_title();

				elseif ( is_tag() ) :
					single_tag_title();

				elseif ( is_author() ) :
					the_post();
					printf( __( 'Author: %s', 'pluginever' ), '' . get_the_author() . '' );
					rewind_posts();

				elseif ( is_day() ) :
					printf( __( 'Day: %s', 'pluginever' ), '<span>' . get_the_date() . '</span>' );

				elseif ( is_month() ) :
					printf( __( 'Month: %s', 'pluginever' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

				elseif ( is_year() ) :
					printf( __( 'Year: %s', 'pluginever' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

				elseif ( is_404() ) :
					_e( 'Page Not Found', 'pluginever' );

				elseif ( is_search() ) :
					printf( __( 'Search Results for: %s', 'pluginever' ), '<span>' . get_search_query() . '</span>' );

				// Title for download archive
				elseif ( is_post_type_archive( 'download' ) && function_exists( 'edd_get_label_plural' ) ) :
					echo edd_get_label_plural();

				// Title for single downloads
				elseif ( is_singular( 'download' ) ) :
					echo get_the_title();

				// Title for download categories and tags
				elseif ( is_tax( array(
						'download', 'download_category',
						'download', 'download_tag' ) ) ) :
							single_term_title();

                elseif ( is_singular( 'docs' ) ) :
                    global $post;
                    _e( 'Documentation', 'pluginever' );
                    echo '<p class="entry-subtitle">';
                    echo get_post_field( 'post_title', $post->post_parent, 'display' );
                    echo '</p>';

                // Title for customer dashboard
				elseif ( is_page_template( 'templates/template-customer-dashboard.php' ) ) :

					if ( is_user_logged_in() ) {
						$current_user = wp_get_current_user();
						echo $current_user->display_name;
					} else {
						_e( 'Customer Login', 'pluginever' );
					}

				else :
					single_post_title();

				endif;
			?>
		</h1>


		<?php
			// Get the category description
			if ( function_exists( 'get_the_archive_description' ) && get_the_archive_description() ) { ?>
				<?php echo get_the_archive_description(); ?>
		<?php } else { ?>
			<?php echo category_description(); ?>
		<?php } ?>


		<?php
			// Get the blog page ID
			if ( ! defined( 'get_the_ID' ) ) {
				$blog_id = get_the_id();
			}
			$page_id = ( 'page' == get_option( 'show_on_front' ) ? get_option( 'page_for_posts' ) : $blog_id );

			// Get post and page subtitles
			if ( is_singular() && function_exists( 'the_subtitle' ) ) { ?>
				<?php the_subtitle( '<p class="entry-subtitle">', '</p>' ); ?>
		<?php } elseif ( is_home() && ! is_front_page() ) { ?>
			<?php
				if ( function_exists( 'get_the_subtitle' ) ) {
				    echo '<p class="entry-subtitle">';
				    	echo get_the_subtitle( $page_id );
				    echo '</p>';
				}
			?>
		<?php } ?>

		<?php do_action( 'pluginever_below_page_titles' ); ?>
	</div><!-- .hero-title-inside -->
</div><!-- .hero-title -->

<?php } ?>
