<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pluginever
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php get_template_part('partials/account', 'header');?>
	<div class="entry-content row">
       <?php get_template_part('partials/account', 'sidebar');?>
		<div class="col-md-9">
			<div class="my-account-content">
				<?php
					the_content();
				?>
			</div>
		</div>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
