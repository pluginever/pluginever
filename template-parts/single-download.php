<?php

$download_id = get_the_ID();
$subtitle    = get_post_meta( get_the_ID(), '_subtitle', true );
// Get purchase button settings
$behavior = get_post_meta( $download_id, '_edd_button_behavior', true );

$hide_button = get_post_meta( $download_id, '_edd_hide_purchase_link', true ) ? 1 : 0;

// If it's a direct purchase show this text
if ( $behavior == 'direct' ) {
    $button_text = __( 'Buy Now', 'checkout' );
} else {
    // if it's an add to cart purchase, get the text from EDD options
    $button_text = ! empty( $edd_options['add_to_cart_text'] ) ? $edd_options['add_to_cart_text'] : __( 'Purchase', 'checkout' );
}
$screenshoots = $behavior = get_post_meta( $download_id, 'screenshoots', true );
global $post;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header download-header">

        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="download-image-wrapper">
                        <?php pluginever_post_thumbnail( 'download-large' ); ?>
                        <?php if ( ! empty( $screenshoots ) ): ?>
                            <div class="schreenshoots-slider">
                                <?php foreach ( $screenshoots as $image_id => $url ): ?>
                                    <?php $thumbnail = wp_get_attachment_image_url($image_id, 'download-slider-thumb');?>
                                    <div><a href="<?php echo $url;?>"><img src="<?php echo $thumbnail;?>" alt="<?php echo $post->post_title;?>"></a></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-7">

                    <?php echo ! empty( $subtitle ) ? sprintf( '<p class="download-subtitle">%s</p>', $subtitle ) : ''; ?>
                    <h1 class="post-title"><?php the_title(); ?></h1>
                    <div class="download-excerpt"> <?php the_excerpt(); ?></div>

                    <?php
                    echo edd_get_purchase_link( array(
                        'download_id' => get_the_ID(),
                        'price'       => false,
                        'direct'      => edd_get_download_button_behavior( get_the_ID() ) == 'direct' ? true : false,
                        'text'        => $button_text
                    ) );
                    ?>

                </div>
            </div>
            <div class="download-header-image"></div>
        </div><!--.container-->

        <div class="download-header-shapes"></div>
        <div class="download-header-curve-shape"></div>
    </header>
    <!--    <div class="download-stat">-->
    <!--        <div class="container">-->
    <!--            <div class="row justify-content-between">-->
    <!---->
    <!--                <div class="col-md-6 download-stat-block-wrap">-->
    <!--                    <div class="col download-stat-block">-->
    <!--                        <span class="number">5326</span> <span class="count-type">Downloads</span>-->
    <!--                    </div>-->
    <!--                    <div class="col download-stat-block">-->
    <!--                        <span class="number">2350</span> <span class="count-type">Premium Users</span>-->
    <!--                    </div>-->
    <!--                    <div class="col download-stat-block">-->
    <!--                        <span class="number">3030</span> <span class="count-type">Active Users</span>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--                <div class="col-md-4 download-customer-satisfaction-block">-->
    <!--                    <img src="-->
    <?php //echo get_stylesheet_directory_uri() . '/images/customer-sasfaction-icon.svg'; ?><!--" alt="pluginever">-->
    <!--                    <div class="download-customer-satisfaction-block-inside">-->
    <!--                        <span>99% Customer</span> <span>Satisfaction</span>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!---->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="entry-content">
        <?php
        the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pluginever' ),
            'after'  => '</div>',
        ) );
        ?>
    </div><!-- .entry-content -->


</article>