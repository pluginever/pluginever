<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pluginever
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="row">
            <div class="footer-top-area">

                <div class="footer-top-left">
                    <?php
                    if ( is_active_sidebar( 'footer-menu-widget' ) ) {
                        dynamic_sidebar( 'footer-menu-widget' );
                    }
                    ?>
                </div>

                <div class="footer-top-right">
                    <div class="footer-subscribe-area">
                        <div class="site-branding">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <img src="/wp-content/uploads/2018/07/pluginver-logo-white.png"
                                     alt="<?php bloginfo( 'name' ); ?>">
                            </a>
                        </div>

                        <h4>Subscribe to our newsletter!</h4>
                        <p>Subscribe to get early access to new plugins, discounts and brief updates about our
                            collection.</p>

                        <div class="footer-subscribe-form">
                            <?php echo do_shortcode( '[mc4wp_form id="212"]' ); ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="site-info">
                <p>&copy; <?php echo date( 'Y' ); ?>
                    <a href="<?php echo site_url(); ?>">PluginEver</a> | We Make WordPress Powerful Just For You</p>
            </div><!-- .site-info -->
        </div>
    </div>

</footer><!-- #colophon -->


</div><!-- #page -->

<?php wp_footer(); ?>

</body></html>
