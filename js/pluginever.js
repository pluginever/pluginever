(function ($) {
    $(document).ready(function () {


        // Center main navigation drop down
        $("#primary-menu li").each(function () {
            if ($(this).find("ul").length > 0) {
                var parent_width = $(this).outerWidth(true);
                var child_width = $(this).find("ul").outerWidth(true);
                var new_width = parseInt((child_width - parent_width) / 2);
                $(this).find("ul").css('margin-left', -new_width + "px");
            }
        });

        //mobile dropdown menu opener
        $("#primary-menu .menu-item-has-children").on('click', function (e) {
            $(this).toggleClass('active');
            e.preventDefault();
        });

        $("#primary-menu .sub-menu").on('click', function (e) {
            e.stopPropagation();
        });

        $('.menu-toggle').on('click', function () {
            if($(window).scrollTop() < 20){
                $('.site-header').toggleClass('is-sticky');
            }
        });


        //sticky menu
        $(window).scroll(handleStickyMenu);

        function handleStickyMenu() {
            if ($(window).scrollTop() > 80) {
                $('.site-header').addClass('is-sticky');
            } else if (($(window).scrollTop() < 50) && !$('.main-navigation').hasClass('toggled')) {
                $('.site-header').removeClass('is-sticky');
            }
        }

        //toggle header on scroll
        $(window).scroll(toggleHeader);

        var lastScrollTop = 0;
        function toggleHeader() {

            if ($(window).width() < 1200) {
                return;
            }


            var st = $(this).scrollTop();
            if (st < 500) {
                return;
            }

            if (st > lastScrollTop) {
                $('.site-header').slideUp('fast');
            } else {
                $('.site-header').slideDown('fast');
            }

            lastScrollTop = st;
        }

        //login and registration handle
        $( '#edd_login_form, #edd_register_form' ).on( 'submit', function( e ) {
            var reCaptcha = $( this ).find( '#g-recaptcha-response' ).val();

            if ( reCaptcha === '' ) {
                e.preventDefault();

                if ( typeof reCaptchaMessage === 'undefined' || reCaptchaMessage === false ) {
                    reCaptchaMessage = true;
    
                    var message = $( '<div class="re-captcha-message"><div class="re-captcha-message-inner">Captcha is required for security check.</div></div>' );
    
                    $( this ).find( '.edd-login-submit, .edd-register-submit' ).prepend( message );

                    message.slideDown( 'fast' );
    
                    setTimeout( function() {
                        message.slideUp( 'fast' ).remove();
                        reCaptchaMessage = false;
                    }, 5000 );
                }
            }
        } );

        //scheenshoot slider
        $('.schreenshoots-slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
        });

        //gallery
        $('.schreenshoots-slider a').simpleLightbox();

    });
})(jQuery);