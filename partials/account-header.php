<?php
global $current_user;
?>
<header class="entry-header text-center">
    <h1 class="entry-title">Account Dashboard</h1>
    <p class="entry-subtitle"><?php echo sprintf('Hi %s (<a href="%s">Logout</a>)',$current_user->display_name,wp_logout_url(site_url()) );?></p>
</header><!-- .entry-header -->
