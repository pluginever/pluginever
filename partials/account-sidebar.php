<?php
global $current_user;
?>
<div class="col-md-3">
    <div class="my-account-customer">
        <div class="my-account-user-info">
            <?php echo get_avatar( $current_user->ID, 65 ); ?>
            <h3><?php echo $current_user->display_name; ?></h3>
        </div>
        <div class="my-account-nav">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'my-account',
                'menu_id'        => 'my-account-menu',
            ) );
            ?>
        </div>
    </div>
</div>
