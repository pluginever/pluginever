<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pluginever
 */

get_header();
?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main">
		<?php if ( have_posts() ) : ?>

            <header class="page-header text-center">
                <h1 class="page-title">Plugins</h1>
            </header>

        <div class="row">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/loop', get_post_type() );

			endwhile;
            echo '</div>';
			pluginever_page_navs();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
