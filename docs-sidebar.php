<div class="wedocs-sidebar wedocs-hide-mobile">
    <?php
    $ancestors = array();
    $root      = $parent = false;

    if ( $post->post_parent ) {
        $ancestors = get_post_ancestors($post->ID);
        $root      = count($ancestors) - 1;
        $parent    = $ancestors[$root];
    } else {
        $parent = $post->ID;
    }

    // var_dump( $parent, $ancestors, $root );
    $walker = new WeDocs_Walker_Docs();
    $children = wp_list_pages( array(
        'title_li'  => '',
        'order'     => 'menu_order',
        'child_of'  => $parent,
        'echo'      => false,
        'post_type' => 'docs',
        'walker'    => $walker
    ) );

    // the_widget( 'WeDocs_Search_Widget' );
    ?>

    <form role="search" method="get" class="search-form pluginever-doc-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ); ?></span>
        <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Documentation Search &hellip;', 'placeholder' ); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ); ?>" />
        <input type="hidden" name="post_type" value="docs" />
        <button type="submit" class="pluginever-doc-search-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
    
    <?php if ( ! is_search() ): ?>
        <h3 class="widget-title"><a href="<?php get_permalink($parent); ?>"><?php echo get_post_field( 'post_title', $parent, 'display' ); ?></a></h3>

        <?php if ($children) { ?>
            <ul class="doc-nav-list">
                <?php echo $children; ?>
            </ul>
        <?php } ?>
    <?php endif; ?>
</div>