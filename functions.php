<?php
/**
 * pluginever functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pluginever
 */
/**
 * Setup functions
 */
require get_template_directory() . '/inc/setup-functions.php';

/**
 * Script functions
 */
require get_template_directory() . '/inc/script-functions.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
/**
 * Widget Functions.
 */
require get_template_directory() . '/inc/widget-functions.php';

/**
 * EDD additions.
 */
require get_template_directory() . '/inc/edd-functions.php';

/**
 * Extra Functions
 */
require get_template_directory() . '/inc/extra-functions.php';

/**
 * metabox Functions
 */
require get_template_directory() . '/inc/metabox-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

